import 'package:get/get.dart';
import 'package:katalog/pages/bottomnavbar/bottomnavbar_controller.dart';
import 'package:katalog/pages/categories/categoriesscreen_controller.dart';
import 'package:katalog/pages/detailscreen/detailscreen_controller.dart';
import 'package:katalog/pages/homescreen/homescreen_controller.dart';

void bindingHomeScreenController() =>
    Get.lazyPut<HomeScreenController>(() => HomeScreenController());

void bindingBottomNavbarController()=>
    Get.lazyPut<BottomNavbarController>(() => BottomNavbarController());

void bindingDetailScreenController()=>
    Get.lazyPut<DetailScreenController>(() => DetailScreenController());

void bindingCategoriesScreenControlelr()=>
Get.lazyPut<CategoriesScreenController>(() => CategoriesScreenController());