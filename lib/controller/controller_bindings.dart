import 'package:get/get.dart';
import 'package:katalog/controller/controller_dependency.dart';

class ControllerBindings extends Bindings {
  @override
  void dependencies() {
    bindingHomeScreenController();
    bindingBottomNavbarController();
    bindingDetailScreenController();
    bindingCategoriesScreenControlelr();
  }
}
