import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:katalog/controller/controller_bindings.dart';
import 'package:katalog/pages/bottomnavbar/bottomnavbar.dart';
import 'package:katalog/pages/detailscreen/detailscreen.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  runApp(MyApp());
}

Map<int, Color> color = {
  50: Color.fromRGBO(252, 243, 246, .1),
  100: Color.fromRGBO(252, 243, 246, .2),
  200: Color.fromRGBO(252, 243, 246, .3),
  300: Color.fromRGBO(252, 243, 246, .4),
  400: Color.fromRGBO(252, 243, 246, .5),
  500: Color.fromRGBO(252, 243, 246, .6),
  600: Color.fromRGBO(252, 243, 246, .7),
  700: Color.fromRGBO(252, 243, 246, .8),
  800: Color.fromRGBO(252, 243, 246, .9),
  900: Color.fromRGBO(252, 243, 246, 1),
};

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    MaterialColor colorCustom = MaterialColor(0XFFfcf3f6, color);
    return GetMaterialApp(
      title: 'Katalog Baju',
      theme: ThemeData(
        primarySwatch: colorCustom,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      debugShowCheckedModeBanner: false,
      initialRoute: '/',
      getPages: [
        GetPage(
          name: '/',
          page: () => BottomNavbarScreen(),
          binding: ControllerBindings(),
        ),
        GetPage(
          name: '/detailscreen',
          page: () => DetailScreen(),
          binding: ControllerBindings(),
        ),
      ],
    );
  }
}
