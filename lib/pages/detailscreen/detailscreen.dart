import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:get/get.dart';
import 'package:katalog/pages/categories/categoriesscreen_controller.dart';
import 'package:katalog/pages/detailscreen/detailscreen_controller.dart';
import 'package:katalog/pages/homescreen/homescreen_controller.dart';

class DetailScreen extends StatelessWidget {
  final DetailScreenController _detailScreenController =
      Get.find<DetailScreenController>();
  final HomeScreenController _homeScreenController =
      Get.find<HomeScreenController>();
  final CategoriesScreenController _categoriesScreenController =
      Get.find<CategoriesScreenController>();

  var one = Get.arguments;

  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height;
    var width = MediaQuery.of(context).size.width;
    return Scaffold(
      backgroundColor: Color(0XFFfcf3f6),
      body: Container(
        height: height,
        width: width,
        color: Color(0XFFfcf3f6),
        child: Column(
          children: [
            ///Item View
            Stack(
              children: [
                Container(
                  height: height * 0.4,
                  width: width,
                  padding: EdgeInsets.symmetric(
                      horizontal: width * 0.05, vertical: width * 0.08),
                  child: Swiper(
                    itemBuilder: (BuildContext context, int index) {
                      return Image.asset(
                        _detailScreenController.listPopular[
                                    _categoriesScreenController.id.value]
                                [_categoriesScreenController.popular][one]
                            ["gambar"],
                        fit: BoxFit.contain,
                      );
                    },
                    itemCount: 3,
                    pagination: SwiperPagination(
                      builder: DotSwiperPaginationBuilder(
                        color: Colors.grey,
                        activeColor: Color(0XFFf98183),
                      ),
                    ),
                    control: SwiperControl(
                      iconNext: null,
                      iconPrevious: null,
                    ),
                  ),
                ),
                Positioned(
                  top: width * 0.12,
                  left: width * 0.03,
                  right: width * 0.03,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      IconButton(
                        icon: Icon(
                          Icons.chevron_left,
                          size: 35,
                          color: Colors.grey[700],
                        ),
                        onPressed: () {
                          Get.back();
                        },
                      ),

                      /// Add item to Favorite
                      IconButton(
                        icon: Obx(
                          () => _detailScreenController.listPopular[
                                          _categoriesScreenController.id.value]
                                          [_categoriesScreenController.popular]
                                          [one]["isFavorite"]
                                      .toString() ==
                                  "true"
                              ? Icon(
                                  Icons.favorite_rounded,
                                  size: 35,
                                  color: Color(0XFFea858e),
                                )
                              : Icon(
                                  Icons.favorite_outline_rounded,
                                  size: 35,
                                  color: Color(0XFFea858e),
                                ),
                        ),
                        onPressed: () {
                          _detailScreenController.addToFav(one);
                        },
                      ),
                    ],
                  ),
                )
              ],
            ),

            ///Deskripsi Produk
            Container(
              height: height * 0.6,
              width: width,
              decoration: BoxDecoration(
                color: Color(0xfffffbfc),
                borderRadius: BorderRadius.circular(width * 0.08),
              ),
              child: Column(
                children: [
                  Expanded(
                    child: Container(
                      padding: EdgeInsets.fromLTRB(
                        width * 0.05,
                        width * 0.05,
                        width * 0.05,
                        0,
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          ///Icon, Nama Produk dan rating
                          headerDeskripsi(context),
                          SizedBox(height: height * 0.02),

                          ///Deskripsi Teks
                          Container(
                            height: height * 0.1,
                            child: AutoSizeText(
                              _detailScreenController.listPopular[
                                          _categoriesScreenController.id.value]
                                      [_categoriesScreenController.popular][one]
                                  ["deskripsi"],
                              style: TextStyle(
                                height: 1.5,
                                wordSpacing: 2,
                                fontSize: 15,
                                fontWeight: FontWeight.w300,
                                color: Colors.grey[600],
                              ),
                              textAlign: TextAlign.start,
                              maxLines: 3,
                            ),
                          ),
                          SizedBox(height: height * 0.02),

                          ///Box item size
                          AutoSizeText(
                            "Item Size",
                            style: TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                          SizedBox(height: height * 0.02),
                          Container(
                            height: height * 0.035,
                            width: width,
                            child: ListView.separated(
                              scrollDirection: Axis.horizontal,
                              itemBuilder: (BuildContext context, int index) {
                                return GestureDetector(
                                  child: itemSizeButton(context, index),
                                  onTap: () {
                                    _detailScreenController.chooseSize(index);
                                  },
                                );
                              },
                              separatorBuilder:
                                  (BuildContext context, int index) {
                                return SizedBox(
                                  width: width * 0.05,
                                );
                              },
                              itemCount:
                                  _detailScreenController.listSize.length,
                            ),
                          ),
                          SizedBox(height: height * 0.03),

                          ///Box Item Size
                          AutoSizeText(
                            "Item Color",
                            style: TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                          SizedBox(height: height * 0.02),
                          Container(
                            height: height * 0.035,
                            width: width,
                            child: ListView.separated(
                              scrollDirection: Axis.horizontal,
                              itemBuilder: (BuildContext context, int index) {
                                return GestureDetector(
                                  child: itemColorButton(context, index),
                                  onTap: () {
                                    _detailScreenController.chooseColor(index);
                                  },
                                );
                              },
                              separatorBuilder:
                                  (BuildContext context, int index) {
                                return SizedBox(
                                  width: width * 0.05,
                                );
                              },
                              itemCount:
                                  _detailScreenController.listColor.length,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),

                  ///
                  footerDeskripsi(context),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  ///Widget untuk menampilkan header pada deskripsi produk yang berupa icon, nama produk, rating, dan jumlah feedback
  Widget headerDeskripsi(context) {
    var width = MediaQuery.of(context).size.width;
    return Row(
      children: [
        Container(
          width: width * 0.65,
          child: Row(
            children: [
              Image.asset("assets/images/uniqlo.png", width: width * 0.06),
              SizedBox(width: width * 0.05),
              AutoSizeText(
                _detailScreenController
                        .listPopular[_categoriesScreenController.id.value]
                    [_categoriesScreenController.popular][one]["judul"],
                style: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.w600,
                ),
              ),
            ],
          ),
        ),
        Container(
          width: width * 0.25,
          child: Row(
            children: [
              Icon(Icons.star_rounded, color: Colors.yellow),
              SizedBox(width: width * 0.01),
              AutoSizeText(
                _detailScreenController
                        .listPopular[_categoriesScreenController.id.value]
                    [_categoriesScreenController.popular][one]["rating"],
                style: TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.bold,
                ),
              ),
              AutoSizeText(
                "(" +
                    _detailScreenController.listPopular[
                                _categoriesScreenController.id.value]
                            [_categoriesScreenController.popular][one]
                        ["jumlahFeedback"] +
                    ")",
                style: TextStyle(
                    fontSize: 10,
                    fontWeight: FontWeight.w600,
                    color: Colors.grey),
                overflow: TextOverflow.ellipsis,
              ),
            ],
          ),
        )
      ],
    );
  }

  ///Widget untuk menampilkan list size yang dapat dipilih
  Widget itemSizeButton(context, index) {
    var height = MediaQuery.of(context).size.height;
    return Container(
      width: height * 0.05,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(10),
      ),
      child: Center(
        child: Obx(
          () => _detailScreenController.listSize[index]["isTrue"] != "true"
              ? AutoSizeText(
                  _detailScreenController.listSize[index]["ukuran"],
                  style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.w600,
                    color: Colors.black,
                  ),
                )
              : AutoSizeText(
                  _detailScreenController.listSize[index]["ukuran"],
                  style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.w600,
                    color: Color(0XFFf98183),
                  ),
                ),
        ),
      ),
    );
  }

  ///Widget untuk menampilkan list color yang dapat dipilih
  Widget itemColorButton(context, index) {
    var width = MediaQuery.of(context).size.width;
    return Container(
      width: width * 0.11,
      height: width * 0.08,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        color: Color(
          _detailScreenController.listColor[index]["warna"],
        ),
      ),
      child: Obx(
        () => _detailScreenController.listColor[index]["isTrue"] == "true"
            ? Icon(
                Icons.check,
                size: 15,
                color: Colors.white,
              )
            : Container(),
      ),
    );
  }

  ///Widget untuk menampilakn footer deskripsi yang berupa Qty, pilih jumlah, total, dan tombol tambah ke keranjang
  Widget footerDeskripsi(context) {
    var height = MediaQuery.of(context).size.height;
    var width = MediaQuery.of(context).size.width;
    return Container(
      height: height * 0.17,
      width: width,
      color: Colors.white,
      padding: EdgeInsets.all(width * 0.05),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                child: Row(
                  children: [
                    AutoSizeText(
                      "Qty",
                      style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.w600,
                          color: Colors.grey),
                    ),
                    SizedBox(width: width * 0.05),

                    ///Tombol untuk mengurangi Qty
                    GestureDetector(
                      child: Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          color: Colors.grey[300],
                        ),
                        child: Obx(
                          () => Icon(
                            Icons.remove,
                            size: 20,
                            color: _detailScreenController.jumlah.value != 1
                                ? Colors.black
                                : Colors.grey,
                          ),
                        ),
                      ),
                      onTap: () {
                        if (_detailScreenController.jumlah.value > 0) {
                          _detailScreenController.decrement(one);
                        }
                      },
                    ),

                    ///Jumlah Qty
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 15),
                      child: Obx(
                        () => AutoSizeText(
                          "${_detailScreenController.jumlah.value}",
                          style: TextStyle(
                              fontSize: 20,
                              color: Colors.black,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                    ),

                    ///Tombol untuk menambah Qty
                    GestureDetector(
                      child: Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          color: Colors.grey[300],
                        ),
                        child: Icon(
                          Icons.add,
                          size: 20,
                        ),
                      ),
                      onTap: () {
                        _detailScreenController.increment(one);
                      },
                    ),
                  ],
                ),
              ),
              Container(
                child: Row(
                  children: [
                    AutoSizeText(
                      "Total",
                      style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.w600,
                          color: Colors.grey),
                    ),
                    SizedBox(width: width * 0.05),

                    ///Total harga
                    Obx(
                      () =>
                          // pengecekan nominal total > 5 character
                          _detailScreenController.listPopular[
                                          _categoriesScreenController.id.value]
                                          [_categoriesScreenController.popular]
                                          [one]["total"]
                                      .toString()
                                      .length >
                                  5
                              ?
                              //pengaturan nominal desimal 2 angka setelah tanda (.)
                              AutoSizeText(
                                  "\$" +
                                      _detailScreenController.listPopular[
                                              _categoriesScreenController.id.value]
                                              [_categoriesScreenController.popular]
                                              [one]["total"]
                                          .toString()
                                          .substring(
                                              0,
                                              _detailScreenController.listPopular[
                                                          _categoriesScreenController
                                                              .id.value]
                                                          [_categoriesScreenController.popular]
                                                          [one]["total"]
                                                      .toString()
                                                      .indexOf(".") +
                                                  3),
                                  style: TextStyle(
                                    fontSize: 20,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.black,
                                  ),
                                )
                              :
                              /* nominal total kurang dari ratusan dollar,
                          jika jumlah hanya satu maka total diambil dari harga diskon
                          */
                              AutoSizeText(
                                  _detailScreenController
                                              .listPopular[_categoriesScreenController.id.value]
                                                  [_categoriesScreenController.popular]
                                                  [one]["total"]
                                              .toString() ==
                                          "0.0"
                                      ?
                                      //ambil dari harga diskon
                                      "\$" +
                                          _detailScreenController
                                              .listPopular[_categoriesScreenController.id.value]
                                                  [_categoriesScreenController.popular]
                                                  [one]["hargaDiskon"]
                                              .toString()
                                      :
                                      //ambil dari harga total
                                      "\$" +
                                          _detailScreenController
                                              .listPopular[_categoriesScreenController.id.value]
                                                  [_categoriesScreenController.popular]
                                                  [one]["total"]
                                              .toString(),
                                  style: TextStyle(
                                    fontSize: 20,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.black,
                                  ),
                                ),
                    ),
                  ],
                ),
              ),
            ],
          ),

          /// Tombol untuk menambah jumlah pada cart
          SizedBox(height: height * 0.02),
          FlatButton(
            height: height * 0.06,
            shape: RoundedRectangleBorder(
              borderRadius: new BorderRadius.circular(30.0),
            ),
            color: Color(0xfff98183),
            onPressed: () {
              _homeScreenController.tambahCart();
              Get.back();
            },
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Icon(
                  Icons.shopping_cart_outlined,
                  size: 25,
                  color: Colors.white,
                ),
                SizedBox(width: width * 0.02),
                AutoSizeText(
                  "Add to chart",
                  style: TextStyle(
                    fontSize: 16,
                    color: Colors.white,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
