import 'package:get/get.dart';
import 'package:katalog/pages/categories/categoriesscreen_controller.dart';

class DetailScreenController extends GetxController {
  final CategoriesScreenController _categoriesScreenController = Get.find<CategoriesScreenController>();
  var listPopular = [
    {
      "hoodie": [
        {
          "judul": "Red Hoodie",
          "hargaAsli": "42.59",
          "hargaDiskon": "32.59",
          "gambar": "assets/images/hoodiepop.png",
          "isFavorite": "false",
          "rating": "4.2",
          "jumlahFeedback": "1600",
          "deskripsi":
              "Hoodie Berwarna merah yang trendi dan nyaman digunakan, dari bahan berkualitas tinggi dan dibuat dengan sepenuh hati",
          "jumlah": 1,
          "total": 0.0,
        },
        {
          "judul": "Black Hoodie",
          "hargaAsli": "42.59",
          "hargaDiskon": "32.59",
          "gambar": "assets/images/hoodieblack.png",
          "isFavorite": "false",
          "rating": "4.9",
          "jumlahFeedback": "1300",
          "deskripsi":
              "Hoodie Berwarna hitam yang trendi dan nyaman digunakan, dari bahan berkualitas tinggi dan dibuat dengan sepenuh hati",
          "jumlah": 1,
          "total": 0.0,
        },
        {
          "judul": "Blue Hoodie",
          "hargaAsli": "42.59",
          "hargaDiskon": "32.59",
          "gambar": "assets/images/hoodieblue.png",
          "isFavorite": "false",
          "rating": "3.4",
          "jumlahFeedback": "1000",
          "deskripsi":
              "Hoodie Berwarna biru yang trendi dan nyaman digunakan, dari bahan berkualitas tinggi dan dibuat dengan sepenuh hati",
          "jumlah": 1,
          "total": 0.0,
        },
        {
          "judul": "White Hoodie",
          "hargaAsli": "42.59",
          "hargaDiskon": "32.59",
          "gambar": "assets/images/hoodiewhite.png",
          "isFavorite": "false",
          "rating": "5.0",
          "jumlahFeedback": "300",
          "deskripsi":
              "Hoodie Berwarna putih yang trendi dan nyaman digunakan, dari bahan berkualitas tinggi dan dibuat dengan sepenuh hati",
          "jumlah": 1,
          "total": 0.0,
        },
      ],
    },
    {
      "boots": [
        {
          "judul": "Yeezy Boots",
          "hargaAsli": "70.99",
          "hargaDiskon": "49.99",
          "gambar": "assets/images/bootpop.png",
          "isFavorite": "false",
          "rating": "3.9",
          "jumlahFeedback": "300",
          "deskripsi":
              "Sepatu Yeezy yang trendi dan nyaman digunakan untuk kebutuhan sehari-hari, terbuat dari bahan berkualitas tinggi",
          "jumlah": 1,
          "total": 0.0,
        },
        {
          "judul": "Nike Boots",
          "hargaAsli": "70.99",
          "hargaDiskon": "49.99",
          "gambar": "assets/images/bootnike.png",
          "isFavorite": "false",
          "rating": "3.2",
          "jumlahFeedback": "450",
          "deskripsi":
              "Sepatu Nike yang trendi dan nyaman digunakan untuk kebutuhan sehari-hari, terbuat dari bahan berkualitas tinggi",
          "jumlah": 1,
          "total": 0.0,
        },
        {
          "judul": "Vans Boots",
          "hargaAsli": "70.99",
          "hargaDiskon": "49.99",
          "gambar": "assets/images/bootpop.png",
          "isFavorite": "false",
          "rating": "4.7",
          "jumlahFeedback": "100",
          "deskripsi":
              "Sepatu Vans yang trendi dan nyaman digunakan untuk kebutuhan sehari-hari, terbuat dari bahan berkualitas tinggi",
          "jumlah": 1,
          "total": 0.0,
        },
        {
          "judul": "New Balance Boots",
          "hargaAsli": "70.99",
          "hargaDiskon": "49.99",
          "gambar": "assets/images/bootnewbalance.png",
          "isFavorite": "false",
          "rating": "3.9",
          "jumlahFeedback": "300",
          "deskripsi":
              "Sepatu New Balance yang trendi dan nyaman digunakan untuk kebutuhan sehari-hari, terbuat dari bahan berkualitas tinggi",
          "jumlah": 1,
          "total": 0.0,
        },
      ],
    },
    {
      "bags": [
        {
          "judul": "Gucci Bag",
          "hargaAsli": "400.29",
          "hargaDiskon": "99.99",
          "gambar": "assets/images/bagpop.png",
          "isFavorite": "false",
          "rating": "5.0",
          "jumlahFeedback": "230",
          "deskripsi":
              "Tas Berwarna gucci yang trendi dan nyaman digunakan, dari bahan berkualitas tinggi dan dibuat dengan sepenuh hati",
          "jumlah": 1,
          "total": 0.0,
        },
        {
          "judul": "Plastic Bag",
          "hargaAsli": "58.29",
          "hargaDiskon": "39.99",
          "gambar": "assets/images/bagplastic.png",
          "isFavorite": "false",
          "rating": "4.5",
          "jumlahFeedback": "90",
          "deskripsi":
              "Tas Berwarna transparan yang trendi dan nyaman digunakan, dari bahan berkualitas tinggi dan dibuat dengan sepenuh hati",
          "jumlah": 1,
          "total": 0.0,
        },
        {
          "judul": "Orange Bag",
          "hargaAsli": "58.29",
          "hargaDiskon": "39.99",
          "gambar": "assets/images/orangebag.png",
          "isFavorite": "false",
          "rating": "4.2",
          "jumlahFeedback": "10",
          "deskripsi":
              "Tas Berwarna orange yang trendi dan nyaman digunakan, dari bahan berkualitas tinggi dan dibuat dengan sepenuh hati",
          "jumlah": 1,
          "total": 0.0,
        },
        {
          "judul": "Leather Bag",
          "hargaAsli": "58.29",
          "hargaDiskon": "39.99",
          "gambar": "assets/images/brownbag.png",
          "isFavorite": "false",
          "rating": "4.7",
          "jumlahFeedback": "20",
          "deskripsi":
              "Tas Berwarna coklat yang trendi dan nyaman digunakan, dari bahan berkualitas tinggi dan dibuat dengan sepenuh hati",
          "jumlah": 1,
          "total": 0.0,
        },
      ],
    },
    {
      "tshirt": [
        {
          "judul": "Black T-shirts",
          "hargaAsli": "50.00",
          "hargaDiskon": "38.99",
          "gambar": "assets/images/tshirtpop.png",
          "isFavorite": "false",
          "rating": "4.7",
          "jumlahFeedback": "300",
          "deskripsi":
              "T-shirt Berwarna hitam yang trendi dan nyaman digunakan, dari bahan berkualitas tinggi dan cocok untuk keseharian",
          "jumlah": 1,
          "total": 0.0,
        },
        {
          "judul": "Dog T-shirts",
          "hargaAsli": "50.00",
          "hargaDiskon": "38.99",
          "gambar": "assets/images/dogshirt.png",
          "isFavorite": "false",
          "rating": "5.0",
          "jumlahFeedback": "1000",
          "deskripsi":
              "T-shirt Bergambar anjing yang trendi dan nyaman digunakan, dari bahan berkualitas tinggi dan cocok untuk keseharian",
          "jumlah": 1,
          "total": 0.0,
        },
        {
          "judul": "Ramen T-shirts",
          "hargaAsli": "50.00",
          "hargaDiskon": "38.99",
          "gambar": "assets/images/ramenshirt.png",
          "isFavorite": "false",
          "rating": "4.8",
          "jumlahFeedback": "3900",
          "deskripsi":
              "T-shirt Berwarna hitam dengan gambar panda yang trendi dan nyaman digunakan, dari bahan berkualitas tinggi dan cocok untuk keseharian",
          "jumlah": 1,
          "total": 0.0,
        },
        {
          "judul": "Yellow T-shirts",
          "hargaAsli": "50.00",
          "hargaDiskon": "38.99",
          "gambar": "assets/images/yellowshirt.png",
          "isFavorite": "false",
          "rating": "4.2",
          "jumlahFeedback": "2300",
          "deskripsi":
              "T-shirt Berwarna kuning yang trendi dan nyaman digunakan, dari bahan berkualitas tinggi dan cocok untuk keseharian",
          "jumlah": 1,
          "total": 0.0,
        }
      ],
    }
  ].obs;

  var listSize = [
    {
      "ukuran": "S",
      "isTrue": "false",
    },
    {
      "ukuran": "M",
      "isTrue": "false",
    },
    {
      "ukuran": "L",
      "isTrue": "false",
    },
    {
      "ukuran": "XL",
      "isTrue": "false",
    },
  ].obs;

  var listColor = [
    {
      "warna": 0xffb1625e,
      "isTrue": "false",
    },
    {
      "warna": 0xff5f7ac0,
      "isTrue": "false",
    },
    {
      "warna": 0xffc28f6b,
      "isTrue": "false",
    },
    {
      "warna": 0xff919191,
      "isTrue": "false",
    },
    {
      "warna": 0xff919191,
      "isTrue": "false",
    },
    {
      "warna": 0xff689159,
      "isTrue": "false",
    },
  ].obs;

  var jumlah = 1.obs;

  ///Function to add favorite item
  void addToFav(index) {
    if (listPopular[_categoriesScreenController.id.value][_categoriesScreenController.popular][index]["isFavorite"] == "true") {
      listPopular[_categoriesScreenController.id.value][_categoriesScreenController.popular][index]["isFavorite"]  = "false";
      //refresh the grid
      listPopular.refresh();
    } else {
      listPopular[_categoriesScreenController.id.value][_categoriesScreenController.popular][index]["isFavorite"] = "true";
      //refresh the grid
      listPopular.refresh();
    }
  }

  ///Function to choose size
  void chooseSize(index) {
    listSize.forEach((element) => element["isTrue"] = "false");
    listSize[index]["isTrue"] = "true";
    listSize.refresh();
  }

  ///Function to choose color
  void chooseColor(index) {
    listColor.forEach((element) => element["isTrue"] = "false");
    listColor[index]["isTrue"] = "true";
    listColor.refresh();
  }
  var hargaDiskon = "200.0".obs;
  ///Function for increment qty and calculate total
  void increment(index) {
    jumlah++;
   listPopular[_categoriesScreenController.id.value][_categoriesScreenController.popular][index]["jumlah"] = jumlah.value;
    listPopular[_categoriesScreenController.id.value][_categoriesScreenController.popular][index]["total"] =
        jumlah.value * double.parse(listPopular[_categoriesScreenController.id.value][_categoriesScreenController.popular][index]["hargaDiskon"]);
        print(listPopular[_categoriesScreenController.id.value][_categoriesScreenController.popular][index]["jumlah"]);
    listPopular.refresh();
  }

  ///Function for decrement qty and calculate total
  void decrement(index) {
    if (jumlah > 1) {
      jumlah--;
      listPopular[_categoriesScreenController.id.value][_categoriesScreenController.popular][index]["jumlah"] = jumlah.value;
      listPopular[_categoriesScreenController.id.value][_categoriesScreenController.popular][index]["total"] =
          jumlah.value * double.parse(listPopular[_categoriesScreenController.id.value][_categoriesScreenController.popular][index]["hargaDiskon"]);
      print(listPopular[_categoriesScreenController.id.value][_categoriesScreenController.popular][index]["jumlah"]);
      listPopular.refresh();
    }
  }
}
