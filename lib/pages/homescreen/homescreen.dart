import 'package:auto_size_text/auto_size_text.dart';
import 'package:badges/badges.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:katalog/pages/categories/categoriesscreen_controller.dart';
import 'package:katalog/pages/detailscreen/detailscreen_controller.dart';
import 'package:katalog/pages/homescreen/homescreen_controller.dart';

class HomeScreen extends StatelessWidget {
  HomeScreen({Key key}) : super(key: key);

  ///Declare GetX controller
  final HomeScreenController _homeScreenController =
      Get.find<HomeScreenController>();
  final DetailScreenController _detailScreenController =
      Get.find<DetailScreenController>();
  final CategoriesScreenController _categoriesScreenController =
      Get.find<CategoriesScreenController>();
  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height;
    var width = MediaQuery.of(context).size.width;
    return Scaffold(
      backgroundColor: Color(0XFFfcf3f6),
      body: SingleChildScrollView(
        child: Stack(
          children: [
            Container(
              width: double.infinity,
              padding: EdgeInsets.symmetric(
                  horizontal: width * 0.05, vertical: width * 0.08),
              color: Color(0XFFfcf3f6),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  // Profile Pict and Cart
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      CircleAvatar(
                        radius: height * 0.025,
                        backgroundImage:
                            AssetImage("assets/images/profile-luch.png"),
                      ),
                      GestureDetector(
                        child: Badge(
                          elevation: 0,
                          badgeColor: Color(0xfff98183),
                          badgeContent: Obx(
                            () => AutoSizeText(
                              _homeScreenController.cart.value.toString(),
                              style: TextStyle(
                                fontSize: 10,
                                color: Colors.white,
                              ),
                            ),
                          ),
                          child: Icon(
                            Icons.shopping_cart_outlined,
                            size: 30,
                          ),
                        ),
                        onTap: () {
                          _homeScreenController.cart.value = 0;
                        },
                      ),
                    ],
                  ),
                  SizedBox(
                    height: height * 0.04,
                  ),
                  // Title and Subtitle
                  AutoSizeText(
                    "Fashion Shop",
                    style: TextStyle(
                      fontSize: 35,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  SizedBox(
                    height: height * 0.015,
                  ),
                  AutoSizeText(
                    "Get Popular Fashion From Home",
                    style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.normal,
                    ),
                  ),
                  SizedBox(height: height * 0.015),
                  //Search Box
                  GestureDetector(
                    child: Container(
                      height: height * 0.06,
                      width: width,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: TextField(
                        cursorColor: Color(0XFF0F6657),
                        maxLengthEnforced: true,
                        enabled: false,
                        style: TextStyle(fontSize: 15),
                        keyboardType: TextInputType.text,
                        decoration: InputDecoration(
                          contentPadding: EdgeInsets.all(height * 0.020),
                          isDense: true,
                          border: InputBorder.none,
                          hintStyle: TextStyle(
                            fontWeight: FontWeight.w400,
                            fontSize: 16,
                            color: Colors.grey,
                          ),
                          hintText: 'Search the clothes you need',
                          prefixIcon: Icon(
                            Icons.search,
                            color: Colors.grey,
                            size: 25,
                          ),
                        ),
                      ),
                    ),
                    onTap: () {
                      // Get.toNamed('/pencarianscreen');
                    },
                  ),
                  SizedBox(height: height * 0.03),
                  //Categories Text, Item on Positioned Widget Below
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      AutoSizeText(
                        "Categories",
                        style: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                      GestureDetector(
                        child: AutoSizeText(
                          "See All",
                          style: TextStyle(
                              color: Color(0xfff98183),
                              fontSize: 16,
                              fontWeight: FontWeight.normal,
                              fontFamily: 'Muli'),
                        ),
                        onTap: () {
                          // if (_homeScreenController.isPasar.value == true) {
                          //   Get.toNamed('/kategoripasarscreen');
                          // } else {
                          //   Get.toNamed('/kategoriwarungscreen');
                          // }
                        },
                      ),
                    ],
                  ),
                  SizedBox(height: height * 0.26),
                  //Popular Fashion Text
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      AutoSizeText(
                        "Popular Fashion",
                        style: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                      GestureDetector(
                        child: AutoSizeText(
                          "See All",
                          style: TextStyle(
                              color: Color(0xfff98183),
                              fontSize: 16,
                              fontWeight: FontWeight.normal,
                              fontFamily: 'Muli'),
                        ),
                        onTap: () {
                          // if (_homeScreenController.isPasar.value == true) {
                          //   Get.toNamed('/kategoripasarscreen');
                          // } else {
                          //   Get.toNamed('/kategoriwarungscreen');
                          // }
                        },
                      ),
                    ],
                  ),
                  //Popular Fashion Grid
                  Container(
                    child: GridView.builder(
                      shrinkWrap: true,
                      physics: new NeverScrollableScrollPhysics(),
                      itemCount: 4,
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 2,
                        crossAxisSpacing: width * 0.005,
                        mainAxisSpacing: height * 0.02,
                        childAspectRatio: height * 0.0009,
                      ),
                      itemBuilder: (BuildContext context, int index) {
                        //Popular fashion Box
                        return GestureDetector(
                          onTap: () {
                            Get.toNamed('/detailscreen', arguments: index);
                          },
                          child: Stack(
                            alignment: Alignment.bottomCenter,
                            children: [
                              popularItemTitleAndPrice(context, index),
                              popularItemImage(context, index),
                            ],
                          ),
                        );
                      },
                    ),
                  ),
                ],
              ),
            ),
            //Categories Scroll Item
            categoriesList(context),
          ],
        ),
      ),
    );
  }

  Widget categoriesList(context) {
    var height = MediaQuery.of(context).size.height;
    var width = MediaQuery.of(context).size.width;
    return Positioned(
      top: height * 0.4,
      child: Container(
        height: width * 0.37,
        width: width,
        color: Colors.transparent,
        child: ListView.separated(
          scrollDirection: Axis.horizontal,
          physics: BouncingScrollPhysics(),
          itemBuilder: (BuildContext context, index) {
            return Obx(
              () => GestureDetector(
                child: Container(
                  width: width * 0.37,
                  decoration: BoxDecoration(
                    color: Color(0XFFfffdfe),
                    borderRadius: BorderRadius.circular(20),
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Image.asset(
                        _categoriesScreenController.listCategories[index]
                            ['gambar'],
                        width: width * 0.15,
                      ),
                      SizedBox(
                        height: height * 0.01,
                      ),
                      AutoSizeText(
                        _categoriesScreenController.listCategories[index]
                            ['judul'],
                        style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ],
                  ),
                ),
                onTap: () {
                  _categoriesScreenController.popular.value =
                      _categoriesScreenController.listCategories[index]
                          ["popular"];
                  _categoriesScreenController.id.value =
                      _categoriesScreenController.listCategories[index]["id"];
                },
              ),
            );
          },
          separatorBuilder: (BuildContext context, index) {
            return SizedBox(
              width: width * 0.03,
            );
          },
          itemCount: _categoriesScreenController.listCategories.length,
        ),
      ),
    );
  }

  ///Widget for item title and price on poupular list
  Widget popularItemTitleAndPrice(context, index) {
    var height = MediaQuery.of(context).size.height;
    var width = MediaQuery.of(context).size.width;
    return Container(
      height: height * 0.13,
      width: width * 0.4,
      padding: EdgeInsets.all(20),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20),
        color: Color(0XFFfffdfe),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Obx(
            () => AutoSizeText(
              _detailScreenController
                      .listPopular[_categoriesScreenController.id.value]
                  [_categoriesScreenController.popular][index]['judul'],
              style: TextStyle(
                fontSize: 15,
                fontWeight: FontWeight.w500,
              ),
            ),
          ),
          SizedBox(height: height * 0.02),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Obx(
                () => AutoSizeText(
                  "\$" +
                      _detailScreenController.listPopular[
                                  _categoriesScreenController.id.value]
                              [_categoriesScreenController.popular][index]
                          ["hargaAsli"],
                  style: TextStyle(
                    fontSize: 13,
                    fontWeight: FontWeight.w500,
                    decoration: TextDecoration.lineThrough,
                    color: Colors.grey,
                  ),
                ),
              ),
              SizedBox(width: width * 0.02),
              Obx(
                () => AutoSizeText(
                  "\$" +
                      _detailScreenController.listPopular[
                                  _categoriesScreenController.id.value]
                              [_categoriesScreenController.popular][index]
                          ["hargaDiskon"],
                  style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.bold,
                    color: Colors.black,
                  ),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }

  ///Widget for item image on pupular list
  Widget popularItemImage(context, index) {
    var height = MediaQuery.of(context).size.height;
    var width = MediaQuery.of(context).size.width;
    return Positioned(
      bottom: height * 0.1,
      child: Container(
        height: height * 0.148,
        width: width * 0.3,
        decoration: BoxDecoration(
          color: Color(0XFFf6f4f5),
          borderRadius: BorderRadius.circular(20),
        ),
        child: Stack(
          alignment: Alignment.center,
          children: [
            Obx(
              () => Image.asset(
                _detailScreenController
                        .listPopular[_categoriesScreenController.id.value]
                    [_categoriesScreenController.popular][index]['gambar'],
                width: width * 0.2,
              ),
            ),
            Positioned(
              top: height * 0.11,
              left: width * 0.22,
              child: GestureDetector(
                child: Obx(
                  () => Icon(
                    _detailScreenController.listPopular[
                                    _categoriesScreenController.id.value]
                                    [_categoriesScreenController.popular][index]
                                    ["isFavorite"]
                                .toString() !=
                            "true"
                        ? Icons.favorite_outline_rounded
                        : Icons.favorite_rounded,
                    size: width * 0.06,
                    color: Color(0XFFea858e),
                  ),
                ),
                onTap: () {
                  _detailScreenController.addToFav(index);
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
