import 'package:get/get.dart';

class HomeScreenController extends GetxController {

  var cart = 0.obs;

  ///Fungsi untuk menambah jumlah cart
  void tambahCart() {
    cart.value = cart.value + 1;
  }
}
