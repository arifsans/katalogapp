import 'package:get/get.dart';

class CategoriesScreenController extends GetxController {
  var id = 0.obs;
  var popular = "hoodie".obs;
  var listCategories = [
    {
      "gambar": "assets/images/hoodie.png",
      "judul": "Hoodie",
      "id": 0,
      "popular": "hoodie",
    },
    {
      "gambar": "assets/images/boots.png",
      "judul": "Boots",
      "id": 1,
      "popular": "boots",
    },
    {
      "gambar": "assets/images/bag.png",
      "judul": "Bags",
      "id": 2,
      "popular": "bags",
    },
    {
      "gambar": "assets/images/tshirt.png",
      "judul": "T-Shirt",
      "id": 3,
      "popular": "tshirt",
    }
  ].obs;
}
