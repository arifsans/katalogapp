import 'dart:collection';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:katalog/pages/favorite/favoritescreen.dart';
import 'package:katalog/pages/history/historyscreen.dart';
import 'package:katalog/pages/homescreen/homescreen.dart';
import 'package:katalog/pages/profile/profilescreen.dart';

class BottomNavbarController extends GetxController{
  var selectedIndex = 0.obs;
  var navigationQueue = ListQueue<int>().obs;
  final List<Widget> pages = [
    HomeScreen(
      key: PageStorageKey('Page1'),
    ),
    FavoriteScreen(
      key: PageStorageKey('Page2'),
    ),
    HistoryScreen(
      key: PageStorageKey('Page3'),
    ),
    ProfileScreen(
      key: PageStorageKey('Page4'),
    ),
  ];
  
  final PageStorageBucket bucket = PageStorageBucket();

  ///Method Yang Pertama Dijalankan Ketika Class Dipanggil
  @override
  void onInit() {
    selectedIndex.value = 0;
    super.onInit();
  }
}