import 'package:flutter/material.dart';
import 'dart:collection';

import 'package:get/get.dart';
import 'package:katalog/pages/bottomnavbar/bottomnavbar_controller.dart';

class BottomNavbarScreen extends StatelessWidget {
  final int initialIndex;

  BottomNavbarScreen({
    this.initialIndex,
    Key key,
  }) : super(key: key);
  BottomNavbarController _bottomNavigationBarController =
      Get.find<BottomNavbarController>();

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        // Use this code if you just want to go back to 0th index

        if (_bottomNavigationBarController.selectedIndex.value == 0)
          return true;

        _bottomNavigationBarController.selectedIndex.value = 0;

        return false;
      },
      child: Scaffold(
        bottomNavigationBar: Obx(
          () => _bottomNavigationBar(
              _bottomNavigationBarController.selectedIndex.value),
        ),
        body: PageStorage(
          child: Obx(
            () => _bottomNavigationBarController
                .pages[_bottomNavigationBarController.selectedIndex.value],
          ),
          bucket: _bottomNavigationBarController.bucket,
        ),
      ),
    );
  }

  ///Show The Bottom NavigationBar On EveryScreen
  Widget _bottomNavigationBar(int selectedIndex) => BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        onTap: (int index) {
          _bottomNavigationBarController.selectedIndex.value = index;
        },
        currentIndex: selectedIndex,
        backgroundColor: Colors.white,
        selectedItemColor: Color(0xfff98183),
        unselectedItemColor: Colors.grey,
        selectedFontSize: 12,
        unselectedFontSize: 10,
        elevation: 0.0,
        items: [
          BottomNavigationBarItem(
            icon: Obx(
              () => _bottomNavigationBarController.selectedIndex.value == 0
                  ? Icon(Icons.home_outlined,
                      size: 25, color: Color(0xfff98183))
                  : Icon(Icons.home_outlined, size: 25),
            ),
            label: "Home",
          ),
          BottomNavigationBarItem(
            icon: Obx(
              () => _bottomNavigationBarController.selectedIndex.value == 1
                  ? Icon(Icons.favorite_border_rounded,
                      size: 25, color: Color(0xfff98183))
                  : Icon(Icons.favorite_border_rounded, size: 25),
            ),
            label: "Favorit",
          ),
          BottomNavigationBarItem(
            icon: Obx(
              () => _bottomNavigationBarController.selectedIndex.value == 2
                  ? Icon(Icons.chat_bubble_outline_rounded,
                      size: 25, color: Color(0xfff98183))
                  : Icon(Icons.chat_bubble_outline_rounded, size: 25),
            ),
            label: "History",
          ),
          BottomNavigationBarItem(
            icon: Obx(
              () => _bottomNavigationBarController.selectedIndex.value == 3
                  ? Icon(Icons.person_outline_rounded,
                      size: 25, color: Color(0xfff98183))
                  : Icon(Icons.person_outline_rounded, size: 25),
            ),
            label: "Profile",
          ),
        ],
      );
}
