# Katalog App

Created By Fauzan Arif Sani On 06 - February - 2021

  - Mobile app with flutter framework
  - Using GetX State Management

# ScreenShot

  ![alt tag](https://i.imgur.com/mj8oAGy.png=40x20)
  
  ![alt tag](https://i.imgur.com/fUKv3K3.png=40x20)
